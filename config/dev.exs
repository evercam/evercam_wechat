import Config

config :evercam_wechat, EvercamWechatWeb.Endpoint,
  http: [port: System.get_env("PORT") || 4000],
  debug_errors: true,
  code_reloader: true,
  check_origin: false,
  watchers: [
    node: [
      "node_modules/webpack/bin/webpack.js",
      "--mode",
      "development",
      "--watch-stdin",
      cd: Path.expand("../assets", __DIR__)
    ]
  ]

# Watch static and templates for browser reloading.
config :evercam_wechat, EvercamWechatWeb.Endpoint,
  live_reload: [
    patterns: [
      ~r{priv/static/.*(js|css|png|jpeg|jpg|gif|svg)$},
      ~r{priv/gettext/.*(po)$},
      ~r{lib/evercam_wechat_web/views/.*(ex)$},
      ~r{lib/evercam_wechat_web/templates/.*(eex)$},
      ~r{lib/evercam_wechat_web/live/.*(ex)$}
    ]
  ]

# Do not include metadata nor timestamps in development logs
config :logger, :console, format: "[$level] $message\n"

# Set a higher stacktrace during development. Avoid configuring such
# in production as building large stacktraces may be expensive.
config :phoenix, :stacktrace_depth, 20

# Initialize plugs at runtime for faster development compilation
config :phoenix, :plug_init_mode, :runtime

# Configure your database
config :evercam_wechat, EvercamWechat.Repo,
  username: "postgres",
  password: "postgres",
  database: "evercam_wechat_dev",
  hostname: "localhost",
  pool_size: 10

# Configure guardian
config :evercam_wechat, EvercamWechatWeb.Guardian,
  issuer: "evercam_wechat",
  secret_key: "quyyvUN5APb/axsAoZp85pDYFE3oQaHaM8GvOjkwe+oG676msASCvtDR0TtUoaRQ"

# Configure phoenix_live_view
config :evercam_wechat, EvercamWechatWeb.Endpoint,
  live_view: [signing_salt: "zQUU1ZeYZgYE89w3vhDwcB2pnKH7uuOR"]

import_config "dev.secret.exs"

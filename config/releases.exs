import Config

config :evercam_wechat, EvercamWechat.Repo, url: System.fetch_env!("EVERCAM_WECHAT_DATABASE_URL")

config :evercam_wechat, EvercamWechatWeb.Guardian,
  issuer: "evercam_wechat",
  secret_key: System.fetch_env!("EVERCAM_WECHAT_GUARDIAN_SECRET_KEY")

config :evercam_wechat, EvercamWechatWeb.Endpoint,
  live_view: [signing_salt: System.fetch_env!("EVERCAM_WECHAT_LIVEVIEW_SIGNING_SALT")]

config :ueberauth, Ueberauth.Strategy.Wechat,
  client_id: System.fetch_env!("EVERCAM_WECHAT_WECHAT_APP_ID"),
  client_secret: System.fetch_env!("EVERCAM_WECHAT_WECHAT_SECRET")

config :evercam_wechat, EvercamWechat.Wechat,
  appid: System.fetch_env!("EVERCAM_WECHAT_WECHAT_APP_ID"),
  secret: System.fetch_env!("EVERCAM_WECHAT_WECHAT_SECRET"),
  token: System.fetch_env!("EVERCAM_WECHAT_WECHAT_TOKEN"),
  encoding_aes_key: System.fetch_env!("EVERCAM_WECHAT_WECHAT_ENCODING_AES_KEY")

import Config

config :evercam_wechat, EvercamWechatWeb.Endpoint,
  http: [compress: true],
  url: [scheme: "https", port: 443],
  load_from_system_env: true,
  cache_static_manifest: "priv/static/cache_manifest.json"

config :phoenix, :serve_endpoints, true

config :logger, level: :info

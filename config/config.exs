# This file is responsible for configuring your application
# and its dependencies with the aid of the Mix.Config module.
#
# This configuration file is loaded before any dependency and
# is restricted to this project.

# General application configuration
import Config

config :evercam_wechat,
  ecto_repos: [EvercamWechat.Repo]

# Configures the endpoint
config :evercam_wechat, EvercamWechatWeb.Endpoint,
  url: [host: "localhost"],
  secret_key_base: "08WHnbXltFNB+WT6S2DtcrKXBreG8bGXoMdSWTrJ/KRuM0csrsVx9m607POatHoa",
  render_errors: [view: EvercamWechatWeb.ErrorView, accepts: ~w(html json)],
  pubsub: [name: EvercamWechat.PubSub, adapter: Phoenix.PubSub.PG2]

# Configures Elixir's Logger
config :logger, :console,
  format: "$time $metadata[$level] $message\n",
  metadata: [:request_id]

# Use Jason for JSON parsing in Phoenix
config :phoenix, :json_library, Jason

# Configure ueberauth
config :ueberauth, Ueberauth,
  providers: [
    wechat: {Ueberauth.Strategy.Wechat, []}
  ]

# Configure wechat
config :wechat,
  httpoison_opts: [recv_timeout: 300_000]

# Configure arc
config :arc, storage: Arc.Storage.Local

# config :evercam_wechat, EvercamWechatWeb.Endpoint,
#   url: [host: "example.com", port: 443, scheme: "https"]

# Import environment specific config. This must remain at the bottom
# of this file so it overrides the configuration defined above.
import_config "#{Mix.env()}.exs"

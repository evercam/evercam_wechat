# EvercamWechat

Evercam wechat official account integration.

## Check wechat official account config

1. Add Server IPs to IP whitelist (`Development -> Basic Configuration`)

![IP whitelist](./screenshots/ip_whitelist.png)

2. Set Webpage authorization domain name (`Settings -> Account Info -> Function setting`)

![Auth domain name](./screenshots/auth_domain_name.png)

## Development

1. Install [yarn](https://yarnpkg.com/en/docs/install).
2. Run `mix setup`to fetch deps, setup database and build assets.
3. Set secrets by `cp config/dev.secret.exs.example config/dev.secret.exs`.
4. Run `mix phx.server`.

## Release
```bash
docker build . -t USERNAME/evercam_wechat:latest
docker push USERNAME/evercam_wechat:latest
```

## Deploy

### Setup runtime configuration

```
# /etc/evercam_wechat

# endpoint 
EVERCAM_WECHAT_HOST=wechat.evercam.io
EVERCAM_WECHAT_PORT=4000
EVERCAM_WECHAT_SECRET_KEY_BASE=(mix phx.gen.secret)

# repo
EVERCAM_WECHAT_DATABASE_URL=ecto://username:password@localhost/evercam_wechat_prod?pool_size=15

# guardian
EVERCAM_WECHAT_GUARDIAN_SECRET_KEY=(mix guardian.gen.secret)

# phoenix_live_view
EVERCAM_WECHAT_LIVEVIEW_SIGNING_SALT=(mix phx.gen.secret 32)

# wechat
EVERCAM_WECHAT_WECHAT_APP_ID=
EVERCAM_WECHAT_WECHAT_SECRET=
EVERCAM_WECHAT_WECHAT_TOKEN=
EVERCAM_WECHAT_WECHAT_ENCODING_AES_KEY=
```
### Run docker container

```bash
docker pull evercam_wechat:latest

docker run --detach --restart=always --network=host --env-file=/etc/evercam_wechat \
  --volume=/srv/evercam_wechat/uploads:/app/uploads:rw USERNAME/evercam_wechat:latest \
  /app/bin/evercam_wechat start
```

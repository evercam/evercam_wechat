$(function() {
  var urls = [];
  $('img.snapshot').map(function(){
    url = window.location.origin + $(this).attr('src'),
    urls.push(url);
  });

  $('img.snapshot').click(function(e) {
    wx.previewImage({
      current: window.location.origin + $(this).attr('src'),
      urls: urls
    });
  })
});

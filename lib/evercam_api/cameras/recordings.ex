defmodule EvercamAPI.Cameras.Recordings do
  @moduledoc """
  Evercam Recordings APIs.
  """

  import EvercamAPI

  @doc """
  Returns the latest snapshot image in base64 format.
  """
  def get_latest_snapshot(client, camera_id) do
    get(client, "cameras/#{camera_id}/recordings/snapshots/latest")
  end
end

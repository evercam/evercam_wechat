defmodule EvercamAPI.Users do
  @moduledoc """
  Evercam User APIs.
  """

  import EvercamAPI

  @doc """
  Returns API credentials of given user.
  """
  def get_api_credentials(id, password) do
    raw_get("users/#{id}/credentials", password: password)
  end
end

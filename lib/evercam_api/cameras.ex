defmodule EvercamAPI.Cameras do
  @moduledoc """
  Evercam Cameras APIs.
  """

  import EvercamAPI

  @doc """
  Returns all public and private cameras.
  """
  def get_cameras(client) do
    get(client, "cameras")
  end

  @doc """
  Returns the latest jpeg image from live camera.
  """
  def get_camera_live_snapshot(client, camera_id) do
    get(client, "cameras/#{camera_id}/live/snapshot")
  end

  @doc """
  Returns the latest thumbnail jpeg image.
  """
  def get_camera_thumbnail(client, camera_id) do
    get(client, "cameras/#{camera_id}/thumbnail")
  end
end

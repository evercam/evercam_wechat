defmodule EvercamAPI.Client do
  @moduledoc false

  @enforce_keys [:api_id, :api_key]
  defstruct [:api_id, :api_key]

  @type t :: %__MODULE__{api_id: binary, api_key: binary}

  @spec new(binary, binary) :: t
  def new(api_id, api_key) do
    %__MODULE__{api_id: api_id, api_key: api_key}
  end
end

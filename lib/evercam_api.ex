defmodule EvercamAPI do
  @moduledoc """
  Evercam API wrapper.
  """

  alias EvercamAPI.Client

  @endpoint "https://media.evercam.io/v2/"

  def raw_get(path, params) do
    @endpoint
    |> URI.merge(path)
    |> HTTPoison.get([], params: params)
    |> handle_response()
  end

  def get(%Client{api_id: api_id, api_key: api_key}, path, params \\ []) do
    params = Enum.into(params, %{api_id: api_id, api_key: api_key})

    @endpoint
    |> URI.merge(path)
    |> HTTPoison.get([], params: params)
    |> handle_response()
  end

  defp handle_response(
         {:ok, %HTTPoison.Response{status_code: 200, body: body, headers: headers}}
       ),
       do: {:ok, handle_response_body(body, headers)}

  defp handle_response({:ok, %HTTPoison.Response{status_code: status_code, body: body}}) do
    case Jason.decode(body) do
      %{"message" => message} ->
        {:error, status_code, message}

      _ ->
        {:error, status_code, ""}
    end
  end

  defp handle_response({:error, %HTTPoison.Error{reason: reason}}) do
    {:error, 502, reason}
  end

  defp handle_response_body(body, headers) do
    headers
    |> content_type()
    |> parse_body(body)
  end

  defp content_type([]), do: ""

  defp content_type([{key, value} | t]) do
    if String.match?(key, ~r/content-type/i) do
      [content_type | _] = String.split(value, ";")
      content_type
    else
      content_type(t)
    end
  end

  defp parse_body("application/json", body), do: Jason.decode!(body)
  defp parse_body(content_type, body), do: {content_type, body}
end

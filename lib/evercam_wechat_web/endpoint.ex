defmodule EvercamWechatWeb.Endpoint do
  use Phoenix.Endpoint, otp_app: :evercam_wechat

  socket "/live", Phoenix.LiveView.Socket

  socket "/socket", EvercamWechatWeb.UserSocket,
    websocket: true,
    longpoll: false

  # Serve at "/" the static files from "priv/static" directory.
  #
  # You should set gzip to true if you are running phx.digest
  # when deploying your static files in production.
  plug Plug.Static,
    at: "/",
    from: :evercam_wechat,
    gzip: false,
    only: ~w(css fonts images js favicon.ico robots.txt MP_verify_RZxWPupASZQSEmOw.txt)

  # serve uploads folder
  plug Plug.Static,
    at: "/uploads",
    from: Path.expand("./uploads"),
    gzip: false

  # Code reloading can be explicitly enabled under the
  # :code_reloader configuration of your endpoint.
  if code_reloading? do
    socket "/phoenix/live_reload/socket", Phoenix.LiveReloader.Socket
    plug Phoenix.LiveReloader
    plug Phoenix.CodeReloader
  end

  plug Plug.RequestId
  plug Plug.Logger

  plug Plug.Parsers,
    parsers: [:urlencoded, :multipart, :json],
    pass: ["*/*"],
    json_decoder: Phoenix.json_library()

  plug Plug.MethodOverride
  plug Plug.Head

  # The session will be stored in the cookie and signed,
  # this means its contents can be read but not tampered with.
  # Set :encryption_salt if you would also like to encrypt it.
  plug Plug.Session,
    store: :cookie,
    key: "_evercam_wechat_key",
    signing_salt: "DUKw+iFr",
    max_age: 86_400

  plug EvercamWechatWeb.Router

  def init(:supervisor, config) do
    if config[:load_from_system_env] do
      port = System.get_env("EVERCAM_WECHAT_PORT")

      case Integer.parse(port) do
        {_int, ""} ->
          host = System.get_env("EVERCAM_WECHAT_HOST")
          secret_key_base = System.get_env("EVERCAM_WECHAT_SECRET_KEY_BASE")
          config = put_in(config[:http][:port], port)
          config = put_in(config[:url][:host], host)
          config = put_in(config[:secret_key_base], secret_key_base)
          {:ok, config}

        :error ->
          {:ok, config}
      end
    else
      {:ok, config}
    end
  end
end

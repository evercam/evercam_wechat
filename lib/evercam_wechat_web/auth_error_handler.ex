defmodule EvercamWechatWeb.AuthErrorHandler do
  @behaviour Guardian.Plug.ErrorHandler

  alias EvercamWechatWeb.Router.Helpers, as: Routes

  @impl true
  def auth_error(conn, {_type, _reason}, _opts) do
    redirect_url = Plug.Conn.request_url(conn)

    conn
    |> Plug.Conn.put_session(:redirect_url, redirect_url)
    |> Phoenix.Controller.redirect(to: Routes.page_path(conn, :index))
  end
end

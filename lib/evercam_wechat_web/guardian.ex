defmodule EvercamWechatWeb.Guardian do
  use Guardian, otp_app: :evercam_wechat

  alias EvercamWechat.Accounts
  alias EvercamWechat.Accounts.User

  import Plug.Conn, only: [delete_session: 2, configure_session: 2]

  def subject_for_token(%User{} = user, _claims) do
    sub = to_string(user.id)
    {:ok, sub}
  end

  def resource_from_claims(%{"sub" => id}) do
    case Accounts.get_user(id) do
      %User{} = user -> {:ok, user}
      nil -> {:error, :unauthorized}
    end
  end

  def login(conn, user) do
    conn
    |> EvercamWechatWeb.Guardian.Plug.sign_in(user)
    |> delete_session(:auth)
    |> delete_session(:redirect_url)
    |> configure_session(renew: true)
  end

  def logout(conn) do
    conn
    |> EvercamWechatWeb.Guardian.Plug.sign_out()
    |> configure_session(drop: true)
  end
end

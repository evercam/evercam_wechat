defmodule EvercamWechatWeb.ViewHelpers do
  @moduledoc """
  Provide functions to use in templates.
  """

  alias EvercamWechatWeb.SimpleTimeZoneList

  @week_days ["Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday"]

  def week_day_mapping do
    Enum.with_index(@week_days, 1)
  end

  def week_day_name(day) do
    Enum.at(@week_days, day + 1)
  end

  def time_zone_mapping do
    SimpleTimeZoneList.mapping()
  end

  def format_time(datetime) do
    Timex.format!(datetime, "%Y-%m-%d %H:%M:%S", :strftime)
  end

  def notification(conn) do
    import Phoenix.Controller, only: [get_flash: 2]
    import Phoenix.HTML.Tag, only: [content_tag: 3]

    cond do
      info = get_flash(conn, :info) ->
        content_tag(:p, info, class: "notification is-success", role: "alert")

      error = get_flash(conn, :error) ->
        content_tag(:p, error, class: "notification is-danger", role: "alert")

      true ->
        ""
    end
  end
end

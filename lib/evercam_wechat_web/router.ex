defmodule EvercamWechatWeb.Router do
  use EvercamWechatWeb, :router

  pipeline :browser do
    plug :accepts, ["html"]
    plug :fetch_session
    plug :fetch_flash
    plug Phoenix.LiveView.Flash
    plug :protect_from_forgery
    plug :put_secure_browser_headers
  end

  pipeline :auth do
    plug Guardian.Plug.Pipeline,
      module: EvercamWechatWeb.Guardian,
      error_handler: EvercamWechatWeb.AuthErrorHandler

    plug Guardian.Plug.VerifySession, claims: %{"typ" => "access"}
    plug Guardian.Plug.VerifyHeader, claims: %{"typ" => "access"}
  end

  pipeline :ensure_auth do
    plug :auth
    plug Guardian.Plug.EnsureAuthenticated
    plug Guardian.Plug.LoadResource
  end

  pipeline :maybe_auth do
    plug :auth
    plug Guardian.Plug.LoadResource, allow_blank: true
  end

  scope "/", EvercamWechatWeb do
    pipe_through [:browser, :ensure_auth, :put_current_user]

    delete "/session", SessionController, :delete
    get "/live_snapshot", LiveSnapshotController, :index

    resources "/subscriptions", SubscriptionController,
      only: [:new, :create, :edit, :update],
      singleton: true

    resources "/snapshot_times", SnapshotTimeController, only: [:index, :show]
    resources "/users", UserController, only: [:edit, :update], singleton: true
  end

  scope "/", EvercamWechatWeb do
    pipe_through [:browser, :maybe_auth, :put_current_user]

    get "/", PageController, :index
    resources "/users", UserController, only: [:new, :create]
  end

  scope "/auth", EvercamWechatWeb do
    pipe_through :browser

    get "/:provider", AuthController, :request
    get "/:provider/callback", AuthController, :callback
    post "/:provider/callback", AuthController, :callback
  end

  scope "/wechat", EvercamWechatWeb do
    resources "/", WechatController, only: [:index, :create]
  end

  defp put_current_user(conn, _opts) do
    current_user = EvercamWechatWeb.Guardian.Plug.current_resource(conn)
    assign(conn, :current_user, current_user)
  end

  pipeline :api do
    plug :accepts, ["json"]
  end
end

defmodule EvercamWechatWeb.SnapshotLive do
  @moduledoc """
  Render live snapshots.
  """

  use Phoenix.LiveView

  require Logger

  alias EvercamAPI.Client
  alias EvercamWechat.Accounts

  def render(assigns) do
    EvercamWechatWeb.SnapshotLiveView.render("snapshots.html", assigns)
  end

  def mount(%{user_id: user_id}, socket) do
    if connected?(socket), do: send(self(), :setup_cameras)

    socket =
      socket
      |> assign_new(:current_user, fn -> Accounts.get_user(user_id) end)
      |> assign(:camera_ids, %{})
      |> assign(:snapshots, %{})

    {:ok, socket}
  end

  def handle_info(:setup_cameras, %{assigns: %{current_user: user}} = socket) do
    client = Client.new(user.evercam_api_id, user.evercam_api_key)

    case EvercamAPI.Cameras.get_cameras(client) do
      {:ok, %{"cameras" => cameras}} ->
        camera_ids =
          Enum.reduce(cameras, [], fn camera, acc ->
            camera_id = camera["id"]
            send(self(), {:send_snapshot, camera_id, client})
            Process.put(camera_id, camera["name"])
            [camera_id | acc]
          end)

        {:noreply, socket |> assign(:camera_ids, camera_ids)}

      {:error, status_code, message} ->
        Logger.error("Get cameras error: #{status_code} - #{message}, will retry.")
        Process.send_after(self(), :setup_cameras, 3000)
        {:noreply, socket}
    end
  end

  def handle_info({:send_snapshot, camera_id, client} = msg, socket) do
    Process.send_after(self(), msg, 10_000)

    case EvercamAPI.Cameras.Recordings.get_latest_snapshot(client, camera_id) do
      {:ok, %{"status" => "ok", "data" => data}} ->
        camera_name = Process.get(camera_id)
        snapshots = Map.put(socket.assigns.snapshots, camera_id, {camera_name, data})
        {:noreply, socket |> assign(:snapshots, snapshots)}

      {:error, status_code, message} ->
        Logger.debug("Camera live snapshot error: #{camera_id}, #{status_code} - #{message}")
        {:noreply, socket}
    end
  end
end

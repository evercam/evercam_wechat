defmodule EvercamWechatWeb.UserController do
  use EvercamWechatWeb, :controller

  alias EvercamWechat.Accounts
  alias EvercamWechat.Accounts.User

  def new(conn, params) do
    if get_session(conn, :auth) do
      changeset = Accounts.change_binding(%User{}, params)
      render(conn, "new.html", changeset: changeset)
    else
      redirect(conn, to: Routes.auth_path(conn, :request, :wechat))
    end
  end

  def create(conn, %{"user" => user_params}) do
    %Ueberauth.Auth{uid: openid, info: %{nickname: nickname}} = get_session(conn, :auth)
    user_params = Map.merge(user_params, %{"openid" => openid, "nickname" => nickname})

    case Accounts.register_user(user_params) do
      {:ok, user} ->
        conn
        |> EvercamWechatWeb.Guardian.login(user)
        |> put_flash(:info, "Bind account succeed.")
        |> redirect(to: Routes.page_path(conn, :index))

      {:error, %{errors: errors} = changeset} ->
        case Keyword.fetch(errors, :evercam_api) do
          {:ok, {error, status_code: status_code, message: message}} ->
            conn
            |> put_flash(:error, "#{error}: #{status_code} - #{message}")
            |> render("new.html", changeset: changeset)

          :error ->
            render(conn, "new.html", changeset: changeset)
        end
    end
  end

  def show(conn, _params) do
    render(conn, "show.html")
  end

  def edit(conn, params) do
    changeset = Accounts.change_user(conn.assigns.current_user, params)
    render(conn, "edit.html", changeset: changeset)
  end

  def update(conn, %{"user" => user_params}) do
    case Accounts.update_user(conn.assigns.current_user, user_params) do
      {:ok, _user} ->
        conn
        |> put_flash(:info, "Update account succeed.")
        |> redirect(to: Routes.user_path(conn, :edit))

      {:error, %{errors: errors} = changeset} ->
        case Keyword.fetch(errors, :evercam_api) do
          {:ok, {error, status_code: status_code, message: message}} ->
            conn
            |> put_flash(:error, "#{error}: #{status_code} - #{message}")
            |> render("edit.html", changeset: changeset)

          :error ->
            render(conn, "edit.html", changeset: changeset)
        end
    end
  end
end

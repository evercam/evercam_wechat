defmodule EvercamWechatWeb.LiveSnapshotController do
  use EvercamWechatWeb, :controller

  import Phoenix.LiveView.Controller
  alias EvercamWechatWeb.SnapshotLive

  def index(%{assigns: %{current_user: user}} = conn, _params) do
    live_render(conn, SnapshotLive, session: %{user_id: user.id})
  end
end

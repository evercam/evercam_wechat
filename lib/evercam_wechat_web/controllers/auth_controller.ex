defmodule EvercamWechatWeb.AuthController do
  use EvercamWechatWeb, :controller
  plug Ueberauth

  require Logger

  alias EvercamWechat.Accounts

  def callback(%{assigns: %{ueberauth_auth: auth}} = conn, %{"provider" => "wechat"}) do
    Logger.debug("auth #{auth.provider}: #{inspect(auth)}")

    case Accounts.get_user_by_openid(auth.uid) do
      nil ->
        conn
        |> put_session(:auth, auth)
        |> put_flash(:error, "Please bind your Evercam account.")
        |> redirect(to: Routes.user_path(conn, :new))

      user ->
        case get_session(conn, :redirect_url) do
          nil ->
            conn
            |> EvercamWechatWeb.Guardian.login(user)
            |> redirect(to: Routes.page_path(conn, :index))

          redirect_url ->
            conn
            |> EvercamWechatWeb.Guardian.login(user)
            |> redirect(external: redirect_url)
        end
    end
  end
end

defmodule EvercamWechatWeb.PageController do
  use EvercamWechatWeb, :controller

  def index(conn, _params) do
    render(conn, "index.html")
  end
end

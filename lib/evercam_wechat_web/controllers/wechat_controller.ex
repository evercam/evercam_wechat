defmodule EvercamWechatWeb.WechatController do
  use EvercamWechatWeb, :controller

  plug Wechat.Plugs.RequestValidator, module: EvercamWechat.Wechat

  plug Wechat.Plugs.MessageParser, [module: EvercamWechat.Wechat] when action in [:create]
  plug :put_user_by_openid when action in [:create]

  alias EvercamWechat.Accounts
  alias EvercamWechat.Accounts.User
  alias EvercamWechat.Wechat

  def index(conn, %{"echostr" => echostr}) do
    text(conn, echostr)
  end

  def create(conn, _params) do
    text(conn, "success")
  end

  defp put_user_by_openid(%{body_params: %{FromUserName: openid}} = conn, _opts) do
    case Accounts.get_user_by_openid(openid) do
      nil ->
        conn
        |> push_bind_account_link()
        |> halt()

      %User{} = user ->
        assign(conn, :user, user)
    end
  end

  defp push_bind_account_link(%{body_params: msg} = conn) do
    link = Routes.auth_url(conn, :request, :wechat)

    reply = %{
      from: msg[:ToUserName],
      to: msg[:FromUserName],
      content: "Account not found, please bind your Evercam account: #{link}"
    }

    msg =
      Phoenix.View.render_to_string(EvercamWechatWeb.WechatView, "bind_account_message.xml",
        reply: reply
      )

    case Wechat.encrypt_message(msg) do
      {:ok, reply} ->
        render(conn, "message.xml", reply: reply)

      {:error, _} ->
        text(conn, msg)
    end
  end
end

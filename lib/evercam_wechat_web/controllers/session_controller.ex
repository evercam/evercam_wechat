defmodule EvercamWechatWeb.SessionController do
  use EvercamWechatWeb, :controller

  def delete(conn, _params) do
    conn
    |> EvercamWechatWeb.Guardian.logout()
    |> redirect(to: Routes.page_path(conn, :index))
  end
end

defmodule EvercamWechatWeb.SubscriptionController do
  use EvercamWechatWeb, :controller

  alias EvercamWechat.Subscriber
  alias EvercamWechat.Subscriber.Subscription
  alias EvercamWechat.Subscriber.SubscriptionSupervisor

  def new(%{assigns: %{current_user: user}} = conn, params) do
    if user.subscription do
      redirect(conn, to: Routes.subscription_path(conn, :show))
    else
      changeset = Subscriber.change_subscription(%Subscription{}, params)
      render(conn, "new.html", changeset: changeset)
    end
  end

  def create(conn, %{"subscription" => subscription_params}) do
    case Subscriber.create_subscription(conn.assigns.current_user, subscription_params) do
      {:ok, subscription} ->
        spawn(fn -> SubscriptionSupervisor.start_subscription_worker(subscription) end)

        conn
        |> put_flash(:info, "Create updates preference succeed!")
        |> redirect(to: Routes.subscription_path(conn, :edit))

      {:error, changeset} ->
        render(conn, "new.html", changeset: changeset)
    end
  end

  def edit(%{assigns: %{current_user: user}} = conn, params) do
    if user.subscription do
      changeset = Subscriber.change_edit_subscription(user.subscription, params)
      render(conn, "edit.html", changeset: changeset)
    else
      redirect(conn, to: Routes.subscription_path(conn, :new))
    end
  end

  def update(%{assigns: %{current_user: user}} = conn, %{"subscription" => subscription_params}) do
    case Subscriber.update_subscription(user.subscription, subscription_params) do
      {:ok, subscription} ->
        spawn(fn -> SubscriptionSupervisor.update_subscription_worker(subscription) end)

        conn
        |> put_flash(:info, "Update updates preference succeed!")
        |> redirect(to: Routes.subscription_path(conn, :edit))

      {:error, changeset} ->
        render(conn, "edit.html", changeset: changeset)
    end
  end
end

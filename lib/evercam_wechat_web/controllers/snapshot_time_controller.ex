defmodule EvercamWechatWeb.SnapshotTimeController do
  use EvercamWechatWeb, :controller

  alias EvercamWechat.Subscriber
  alias Timex.Timezone

  def index(%{assigns: %{current_user: user}} = conn, _) do
    snapshot_times = Subscriber.snapshot_times_by_user(user)

    snapshot_times =
      if user.subscription do
        Enum.map(snapshot_times, &convert_time_by_timezone(&1, user.subscription.timezone))
      else
        snapshot_times
      end

    render(conn, "index.html", snapshot_times: snapshot_times)
  end

  def show(%{assigns: %{current_user: user}} = conn, %{"id" => id}) do
    snapshot_time = Subscriber.get_user_snapshot_time(conn.assigns.current_user, id)
    snapshot_time = convert_time_by_timezone(snapshot_time, user.subscription.timezone)
    render(conn, "show.html", snapshot_time: snapshot_time)
  end

  defp convert_time_by_timezone(snapshot_time, nil), do: snapshot_time

  defp convert_time_by_timezone(snapshot_time, timezone) do
    time = Timezone.convert(snapshot_time.time, timezone)
    %{snapshot_time | time: time}
  end
end

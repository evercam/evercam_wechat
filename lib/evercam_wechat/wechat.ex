defmodule EvercamWechat.Wechat do
  @moduledoc """
  Wechat implementation for evercam official account.
  """

  use Wechat, otp_app: :evercam_wechat

  @subscription_template_id "v5x5H6z5HN_oq-py0YT2ofB7xvIExxZhPylqvVTiGrg"

  def appid do
    client()[:appid]
  end

  def upload_image(content_type, binary) do
    path = tmp_path(content_type)
    File.write!(path, binary)

    try do
      case Wechat.Media.upload(client(), path, :image) do
        {:ok, %{"media_id" => media_id}} ->
          {:ok, media_id}

        {:error, error} ->
          {:error, error}
      end
    after
      File.rm!(path)
    end
  end

  def send_image(openid, media_id) do
    Wechat.Message.custom_send_image(client(), openid, media_id)
  end

  def send_typing_status(openid) do
    Wechat.Message.custom_typing_start(client(), openid)
  end

  # Wechat guess file type from file extension
  defp tmp_path("image/jpeg") do
    path = Base.url_encode64(:crypto.strong_rand_bytes(32), padding: false)
    Path.join(System.tmp_dir!(), path) <> ".jpeg"
  end

  def notify_subscription(openid, snapshot_count, snapshot_time) do
    data = %{
      keyword1: %{
        value: snapshot_count
      },
      keyword2: %{
        value: snapshot_time
      },
      first: %{
        value: """
        Hi, this is a reminder for your subscription.
        Click "Updates" menu item to view latest snapshots.
        """
      },
      remark: %{
        value: """
        Images Received: #{snapshot_count}
        Receive Time: #{snapshot_time}
        """
      }
    }

    Wechat.Message.template_send(client(), openid, @subscription_template_id, data)
  end
end

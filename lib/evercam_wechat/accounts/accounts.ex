defmodule EvercamWechat.Accounts do
  @moduledoc false

  require Logger

  alias EvercamAPI.Client
  alias EvercamWechat.Accounts.User
  alias EvercamWechat.Repo

  def get_user(id) do
    User
    |> Repo.get(id)
    |> Repo.preload(:subscription)
  end

  def get_user_by_openid(openid) do
    Repo.get_by(User, openid: openid)
  end

  @doc """
  Changeset for binding form.
  """
  def change_binding(user, params) do
    User.binding_changeset(user, params)
  end

  def change_user(user, params) do
    User.changeset(user, params)
  end

  def register_user(%{"openid" => openid} = params) do
    case get_user_by_openid(openid) do
      nil ->
        %User{}
        |> User.changeset(params)
        |> Repo.insert()

      user ->
        {:ok, user}
    end
  end

  def update_user(user, params) do
    user
    |> User.changeset(params)
    |> Repo.update()
  end

  @doc """
  Build Evercam API client from user.
  """
  def evercam_api_client(%User{evercam_api_id: id, evercam_api_key: key}) do
    Client.new(id, key)
  end
end

defmodule EvercamWechat.Accounts.User do
  use Ecto.Schema
  import Ecto.Changeset

  alias EvercamWechat.Subscriber.{SnapshotTime, Subscription}

  schema "users" do
    field :evercam_api_id, :string
    field :evercam_api_key, :string
    field :nickname, :string
    field :openid, :string

    field :evercam_id, :string, virtual: true
    field :evercam_pwd, :string, virtual: true

    has_one :subscription, Subscription
    has_many :snapshot_times, SnapshotTime

    timestamps()
  end

  def binding_changeset(user, attrs) do
    user
    |> cast(attrs, [:evercam_id, :evercam_pwd])
    |> validate_required([:evercam_id, :evercam_pwd])
  end

  def changeset(user, attrs) do
    user
    |> binding_changeset(attrs)
    |> put_evercam_api_fields()
    |> cast(attrs, [:evercam_api_id, :evercam_api_key, :openid, :nickname])
    |> validate_required([:evercam_api_id, :evercam_api_key, :openid, :nickname])
    |> unique_constraint(:openid)
  end

  # Request evercam api_id and api_key by user credential filled in binding form.
  defp put_evercam_api_fields(%{valid?: true, changes: changes} = changeset) do
    case EvercamAPI.Users.get_api_credentials(changes.evercam_id, changes.evercam_pwd) do
      {:ok, %{"api_id" => api_id, "api_key" => api_key}} ->
        change(changeset, %{evercam_api_id: api_id, evercam_api_key: api_key})

      {:error, status_code, message} ->
        add_error(changeset, :evercam_api, "Evercam API error",
          status_code: status_code,
          message: message
        )
    end
  end

  defp put_evercam_api_fields(changeset), do: changeset
end

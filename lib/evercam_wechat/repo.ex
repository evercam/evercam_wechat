defmodule EvercamWechat.Repo do
  use Ecto.Repo,
    otp_app: :evercam_wechat,
    adapter: Ecto.Adapters.Postgres
end

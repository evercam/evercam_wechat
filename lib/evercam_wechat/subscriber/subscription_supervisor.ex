defmodule EvercamWechat.Subscriber.SubscriptionSupervisor do
  @moduledoc """
  Subscription supervisor manages dynamically generated subscription workers.

  `start_subscription_worker/1` is called when subscription is created.
  `update_subscription_worker/1` must be called if subscription is updated.
  """

  use DynamicSupervisor

  alias EvercamWechat.Subscriber
  alias EvercamWechat.Subscriber.SubscriptionServer

  def start_link(_) do
    DynamicSupervisor.start_link(__MODULE__, :ok, name: __MODULE__)
  end

  def init(:ok) do
    Task.start_link(&init_workers/0)
    DynamicSupervisor.init(strategy: :one_for_one)
  end

  def init_workers do
    Subscriber.all_enabled_subscriptions() |> Enum.each(&start_subscription(&1))
  end

  def start_subscription_worker(subscription) do
    start_subscription(subscription)
  end

  def update_subscription_worker(subscription) do
    "subscription-worker-#{subscription.id}"
    |> String.to_atom()
    |> Process.whereis()
    |> update_subscription(subscription)
  end

  defp start_subscription(subscription) do
    child_spec = {SubscriptionServer, subscription}
    DynamicSupervisor.start_child(__MODULE__, child_spec)
  end

  defp update_subscription(worker_pid, subscription) do
    if worker_pid != nil do
      DynamicSupervisor.terminate_child(__MODULE__, worker_pid)
    end

    if subscription.enabled do
      start_subscription(subscription)
    end
  end
end

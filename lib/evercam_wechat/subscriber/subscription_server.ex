defmodule EvercamWechat.Subscriber.SubscriptionServer do
  @moduledoc """
  Worker for subscription.
  """

  use GenServer

  require Logger

  alias EvercamWechat.Accounts
  alias EvercamWechat.Subscriber
  alias EvercamWechat.Wechat

  def start_link(subscription) do
    name = "subscription-worker-#{subscription.id}" |> String.to_atom()
    GenServer.start_link(__MODULE__, subscription, name: name)
  end

  def init(subscription) do
    :timer.send_interval(1_000, :poll)
    {:ok, subscription}
  end

  def handle_info(:poll, subscription) do
    %{notify_days: notify_days, notify_time: %{hour: hour, minute: minute}} = subscription

    now =
      if subscription.timezone do
        Timex.now(subscription.timezone)
      else
        DateTime.utc_now()
      end

    week_day = Date.day_of_week(now)

    if week_day in notify_days && now.hour == hour && now.minute == minute && now.second == 0 do
      user = Accounts.get_user(subscription.user_id)
      Logger.info("Saving snapshots for user-#{user.id} at #{now}")
      save_snapshots(user, now)
    end

    {:noreply, subscription}
  end

  defp save_snapshots(user, now, retries \\ 3)

  defp save_snapshots(user, now, 0) do
    Logger.error("Excceed save_snapshots retry limits! user-#{user.id}, #{now}")
  end

  defp save_snapshots(user, now, retries) do
    client = Accounts.evercam_api_client(user)

    case EvercamAPI.Cameras.get_cameras(client) do
      {:ok, %{"cameras" => cameras}} ->
        snapshot_count =
          cameras
          |> Enum.map(&snapshot_by_camera(&1, client))
          |> Enum.map(&Task.await(&1, 60_000))
          |> Subscriber.save_snapshots(user, now)

        Wechat.notify_subscription(user.openid, snapshot_count, format_time(now))

      {:error, status_code, message} ->
        Logger.error("Camera get cameras error: #{status_code} - #{message}")
        save_snapshots(user, now, retries - 1)
    end
  end

  defp snapshot_by_camera(%{"id" => camera_id}, client) do
    Task.async(fn ->
      case EvercamAPI.Cameras.get_camera_thumbnail(client, camera_id) do
        {:ok, {content_type, binary}} ->
          {camera_id, content_type, binary}

        {:error, status_code, message} ->
          Logger.debug("Camera live snapshot error: #{camera_id}, #{status_code} - #{message}")
      end
    end)
  end

  defp format_time(datetime) do
    Timex.format!(datetime, "%Y-%m-%d %H:%M:%S", :strftime)
  end
end

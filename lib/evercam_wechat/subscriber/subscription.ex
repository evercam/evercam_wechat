defmodule EvercamWechat.Subscriber.Subscription do
  use Ecto.Schema
  import Ecto.Changeset

  alias EvercamWechat.Accounts.User

  schema "subscriptions" do
    field :notify_days, {:array, :integer}
    field :notify_time, :time
    field :notify_time_select, {:map, :integer}, virtual: true
    field :timezone, :string
    field :enabled, :boolean
    belongs_to :user, User

    timestamps()
  end

  @doc false
  def changeset(subscription, attrs) do
    subscription
    |> cast(attrs, [:notify_days, :notify_time_select, :timezone, :enabled])
    |> validate_required([:notify_days, :notify_time_select, :timezone, :enabled])
    |> put_notify_time()
  end

  defp put_notify_time(
         %{valid?: true, changes: %{notify_time_select: %{"hour" => hour, "minute" => minute}}} =
           changeset
       ) do
    {:ok, notify_time} = Time.new(hour, minute, 0)
    put_change(changeset, :notify_time, notify_time)
  end

  defp put_notify_time(%{valid?: false} = changeset) do
    changeset
  end
end

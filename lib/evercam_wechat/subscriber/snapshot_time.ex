defmodule EvercamWechat.Subscriber.SnapshotTime do
  use Ecto.Schema
  import Ecto.Changeset

  alias EvercamWechat.Accounts.User
  alias EvercamWechat.Subscriber.Snapshot

  schema "snapshot_times" do
    field :time, :utc_datetime
    belongs_to :user, User
    has_many :snapshots, Snapshot

    timestamps()
  end

  @doc false
  def changeset(snapshot_time, attrs) do
    snapshot_time
    |> cast(attrs, [:time])
    |> validate_required([:time])
  end
end

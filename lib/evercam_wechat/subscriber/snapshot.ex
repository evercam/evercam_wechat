defmodule EvercamWechat.Subscriber.Snapshot do
  use Ecto.Schema
  use Arc.Ecto.Schema

  import Ecto.Changeset

  alias EvercamWechat.Subscriber.SnapshotTime

  schema "snapshots" do
    field :camera_id, :string
    field :content_type, :string
    field :file, EvercamWechat.SnapshotFile.Type
    belongs_to :snapshot_time, SnapshotTime

    timestamps()
  end

  @doc false
  def changeset(snapshot, attrs) do
    snapshot
    |> cast(attrs, [:camera_id, :content_type])
    |> cast_attachments(attrs, [:file])
    |> validate_required([:camera_id, :camera_id, :file])
  end
end

defmodule EvercamWechat.Subscriber do
  @moduledoc false

  import Ecto.Query

  alias EvercamWechat.Repo
  alias EvercamWechat.Subscriber.{Snapshot, SnapshotTime, Subscription}

  def change_subscription(subscription, params) do
    Subscription.changeset(subscription, params)
  end

  def change_edit_subscription(subscription, params) do
    subscription
    |> Ecto.Changeset.change(params)
    |> format_notify_time_select()
  end

  defp format_notify_time_select(%{data: %{notify_time: notify_time}} = changeset) do
    notify_time_select = %{hour: notify_time.hour, minute: notify_time.minute}
    Ecto.Changeset.put_change(changeset, :notify_time_select, notify_time_select)
  end

  def create_subscription(user, params) do
    %Subscription{}
    |> Subscription.changeset(params)
    |> put_user(user)
    |> Repo.insert()
  end

  defp put_user(changeset, user) do
    Ecto.Changeset.put_assoc(changeset, :user, user)
  end

  def update_subscription(subscription, params) do
    subscription
    |> Subscription.changeset(params)
    |> Repo.update()
  end

  def all_enabled_subscriptions do
    Repo.all(from s in Subscription, where: [enabled: true], preload: :user)
  end

  def save_snapshots(snapshots, user, time) do
    snapshot_time =
      %SnapshotTime{}
      |> SnapshotTime.changeset(%{time: time})
      |> put_user(user)
      |> Repo.insert!()

    snapshots
    |> Enum.map(&save_snapshot(&1, snapshot_time))
    |> Enum.count(fn s -> s != :none end)
  end

  defp save_snapshot({camera_id, content_type, data}, snapshot_time) do
    file = %{__struct__: Plug.Upload, filename: Ecto.UUID.generate(), binary: data}

    %Snapshot{}
    |> Snapshot.changeset(%{content_type: content_type, file: file, camera_id: camera_id})
    |> put_snapshot_time(snapshot_time)
    |> Repo.insert!()
  end

  defp save_snapshot(_, _), do: :none

  def put_snapshot_time(changeset, snapshot_time) do
    Ecto.Changeset.put_assoc(changeset, :snapshot_time, snapshot_time)
  end

  def snapshot_times_by_user(user) do
    query = from SnapshotTime, where: [user_id: ^user.id], order_by: [desc: :id]
    Repo.all(query)
  end

  def get_user_snapshot_time(user, id) do
    SnapshotTime
    |> Repo.get_by!(user_id: user.id, id: id)
    |> Repo.preload(:snapshots)
  end
end

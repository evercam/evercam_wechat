defmodule EvercamWechat.Repo.Migrations.CreateSnapshotTimes do
  use Ecto.Migration

  def change do
    create table(:snapshot_times) do
      add :time, :utc_datetime
      add :user_id, references(:users, on_delete: :nothing)

      timestamps()
    end

    create index(:snapshot_times, [:user_id])
  end
end

defmodule EvercamWechat.Repo.Migrations.CreateSubscriptions do
  use Ecto.Migration

  def change do
    create table(:subscriptions) do
      add :notify_days, {:array, :integer}
      add :notify_time, :time
      add :timezone, :string
      add :user_id, references(:users, on_delete: :delete_all)

      timestamps()
    end

    create unique_index(:subscriptions, [:user_id])
  end
end

defmodule EvercamWechat.Repo.Migrations.AddCameraIdToSnapshots do
  use Ecto.Migration

  def change do
    alter table("snapshots") do
      add :camera_id, :string
    end
  end
end

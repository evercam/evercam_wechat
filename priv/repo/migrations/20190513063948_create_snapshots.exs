defmodule EvercamWechat.Repo.Migrations.CreateSnapshots do
  use Ecto.Migration

  def change do
    create table(:snapshots) do
      add :content_type, :string
      add :data, :binary
      add :snapshot_time_id, references(:snapshot_times, on_delete: :delete_all)

      timestamps()
    end

    create index(:snapshots, [:snapshot_time_id])
  end
end

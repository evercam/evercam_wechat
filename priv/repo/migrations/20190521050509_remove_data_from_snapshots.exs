defmodule EvercamWechat.Repo.Migrations.RemoveDataFromSnapshots do
  use Ecto.Migration

  def change do
    alter table("snapshots") do
      remove :data
    end
  end
end

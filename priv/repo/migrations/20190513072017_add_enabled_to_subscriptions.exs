defmodule EvercamWechat.Repo.Migrations.AddEnabledToSubscriptions do
  use Ecto.Migration

  def change do
    alter table("subscriptions") do
      add :enabled, :boolean, default: false
    end
  end
end

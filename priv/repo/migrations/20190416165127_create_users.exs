defmodule EvercamWechat.Repo.Migrations.CreateUsers do
  use Ecto.Migration

  def change do
    create table(:users) do
      add :nickname, :string
      add :openid, :string
      add :evercam_api_id, :string
      add :evercam_api_key, :string

      timestamps()
    end

    create unique_index(:users, [:openid])
  end
end

defmodule EvercamWechat.Repo.Migrations.AddFileToSnapshots do
  use Ecto.Migration

  def change do
    alter table("snapshots") do
      add :file, :string
    end
  end
end

alias EvercamWechat.{Repo, Accounts, Subscriber}

alias EvercamWechat.Accounts.User
alias EvercamWechat.Subscriber.{Subscription, Snapshot, SnapshotTime}

import_if_available(Ecto.Query)
import_if_available(Ecto.Changeset)
